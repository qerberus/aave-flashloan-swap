// SPDX-License-Identifier: agpl-3.0
pragma solidity 0.6.12;
pragma experimental ABIEncoderV2;

// Uniswap Adapter
import {BaseUniswapAdapter} from '@aave/protocol-v2/contracts/adapters/BaseUniswapAdapter.sol';

// Interfaces
import {
  ILendingPoolAddressesProvider
} from '@aave/protocol-v2/contracts/interfaces/ILendingPoolAddressesProvider.sol';
import {ILendingPool} from '@aave/protocol-v2/contracts/interfaces/ILendingPool.sol';
import {IUniswapV2Router02} from '@aave/protocol-v2/contracts/interfaces/IUniswapV2Router02.sol';
import {IERC20} from '@aave/protocol-v2/contracts/dependencies/openzeppelin/contracts/IERC20.sol';

import 'hardhat/console.sol';

/**
 * @title FlashLiquidationAdapter
 * @notice Flash Liquidation that uses Uniswap V2 Adapter to swap released collateral during Aave V2 liquidations.
 * @dev You can check latest source at Aave Protocol V2 repository: https://github.com/aave/protocol-v2/blob/master/contracts/adapters/FlashLiquidationAdapter.sol
 * @author Aave
 **/
contract FlashLiquidationAdapter is BaseUniswapAdapter {
  struct LiquidationParams {
    address collateralAsset;
    address borrowedAsset;
    address depositAsset;
    address user;
    uint256 debtToCover;
    bool useEthPath;
  }

  struct LiquidationCallLocalVars {
    uint256 initFlashBorrowedBalance;
    uint256 diffFlashBorrowedBalance;
    uint256 initCollateralBalance;
    uint256 diffCollateralBalance;
    uint256 flashLoanDebt;
    uint256 soldAmount;
    uint256 boughtAmount;
    address aToken;
    uint256 remainingTokens;
    uint256 borrowedAssetLeftovers;
  }

  constructor(
    ILendingPoolAddressesProvider addressesProvider,
    IUniswapV2Router02 uniswapRouter,
    address wethAddress
  ) public BaseUniswapAdapter(addressesProvider, uniswapRouter, wethAddress) {}

  /**
   * @dev Liquidate a non-healthy position collateral-wise, with a Health Factor below 1, using Flash Loan and Uniswap to repay flash loan premium.
   * - The caller (liquidator) with a flash loan covers `debtToCover` amount of debt of the user getting liquidated, and receives
   *   a proportionally amount of the `collateralAsset` plus a bonus to cover market risk minus the flash loan premium.
   * @param assets Address of asset to be swapped
   * @param amounts Amount of the asset to be swapped
   * @param premiums Fee of the flash loan
   * @param initiator Address of the caller
   * @param params Additional variadic field to include extra params. Expected parameters:
   *   address collateralAsset The collateral asset to release and will be exchanged to pay the flash loan premium
   *   address borrowedAsset The asset that must be covered
   *   address depositAsset The asset to deposit after the self-liquidation
   *   address user The borrower address with a Health Factor below 1
   *   uint256 debtToCover The amount of debt to cover
   *   bool useEthPath Use WETH as connector path between the collateralAsset and borrowedAsset at Uniswap
   */
  function executeOperation(
    address[] calldata assets,
    uint256[] calldata amounts,
    uint256[] calldata premiums,
    address initiator,
    bytes calldata params
  ) external override returns (bool) {
    // this function must be called by LENDING_POOL
    require(msg.sender == address(LENDING_POOL), 'CALLER_MUST_BE_LENDING_POOL');

    LiquidationParams memory decodedParams = _decodeParams(params);

    require(assets.length == 1 && assets[0] == decodedParams.borrowedAsset, 'INCONSISTENT_PARAMS');

    // In this fuction, liquidation + swap will happen
    // liquidation with debt token of borrowedAsset: LENDING_POOL.liquidationCall
    // swap from released collateralAsset to borrowedAsset: _swapTokensForExactTokens
    // swap from borrowedAsset to depositAsset: _swapExactTokensForTokens
    _liquidateAndSwapAndDeposit(
      decodedParams.collateralAsset,
      decodedParams.borrowedAsset,
      decodedParams.depositAsset,
      decodedParams.user,
      decodedParams.debtToCover,
      decodedParams.useEthPath,
      amounts[0],
      premiums[0],
      initiator
    );

    return true;
  }

  /**
   * @dev
   * @param collateralAsset The collateral asset to release and will be exchanged to pay the flash loan premium
   * @param borrowedAsset The asset that must be covered
   * @param user The borrower address with a Health Factor below 1
   * @param debtToCover The amount of debt to coverage, can be max(-1) to liquidate all possible debt
   * @param useEthPath true if the swap needs to occur using ETH in the routing, false otherwise
   * @param flashBorrowedAmount Amount of asset requested at the flash loan to liquidate the user position
   * @param premium Fee of the requested flash loan
   * @param initiator Address of the caller
   */
  function _liquidateAndSwapAndDeposit(
    address collateralAsset,
    address borrowedAsset,
    address depositAsset,
    address user,
    uint256 debtToCover,
    bool useEthPath,
    uint256 flashBorrowedAmount,
    uint256 premium,
    address initiator
  ) internal {
    LiquidationCallLocalVars memory vars;
    vars.initCollateralBalance = IERC20(collateralAsset).balanceOf(address(this));
    if (collateralAsset != borrowedAsset) {
      vars.initFlashBorrowedBalance = IERC20(borrowedAsset).balanceOf(address(this));

      // Track leftover balance to rescue funds in case of external transfers into this contract
      vars.borrowedAssetLeftovers = vars.initFlashBorrowedBalance.sub(flashBorrowedAmount);
    }
    vars.flashLoanDebt = flashBorrowedAmount.add(premium);

    // Approve LendingPool to use debt token for liquidation
    // That means LendingPool will own the debt of borrowedAsset.
    IERC20(borrowedAsset).approve(address(LENDING_POOL), debtToCover);

    // Liquidate the user position and release the underlying collateral
    /**
     * @dev Users can invoke this function to liquidate an undercollateralized position.
     * @param collateral The address of the collateral to liquidated
     * @param borrowed The address of the borrowed asset
     * @param user The address of the borrower
     * @param debtToCover The amount of borrowed asset that the liquidator wants to repay
     * @param receiveAToken true if the liquidators wants to receive the aTokens, false if
     * he wants to receive the underlying asset directly
     **/
    LENDING_POOL.liquidationCall(collateralAsset, borrowedAsset, user, debtToCover, false);

    // Discover the liquidated tokens
    uint256 collateralBalanceAfter = IERC20(collateralAsset).balanceOf(address(this));

    // Track only collateral released, not current asset balance of the contract
    vars.diffCollateralBalance = collateralBalanceAfter.sub(vars.initCollateralBalance);

    if (collateralAsset != borrowedAsset) {
      // Discover flash loan balance after the liquidation
      uint256 flashBorrowedAssetAfter = IERC20(borrowedAsset).balanceOf(address(this));

      // Use only flash loan borrowed assets, not current asset balance of the contract
      vars.diffFlashBorrowedBalance = flashBorrowedAssetAfter.sub(vars.borrowedAssetLeftovers);

      // Swap released collateral into the debt asset, to repay the flash loan
      /**
       * _swapTokensForExactTokens:
       * @dev Receive an exact amount `amountToReceive` of `assetToSwapTo` tokens for as few `assetToSwapFrom` tokens as
       * possible.
       * @param assetToSwapFrom Origin asset
       * @param assetToSwapTo Destination asset
       * @param maxAmountToSwap Max amount of `assetToSwapFrom` allowed to be swapped
       * @param amountToReceive Exact amount of `assetToSwapTo` to receive
       * @return the amount swapped
      **/
      vars.soldAmount = _swapTokensForExactTokens(
        collateralAsset,
        borrowedAsset,
        vars.diffCollateralBalance,
        vars.flashLoanDebt.sub(vars.diffFlashBorrowedBalance),
        useEthPath
      );
      vars.remainingTokens = vars.diffCollateralBalance.sub(vars.soldAmount);
    } else {
      vars.remainingTokens = vars.diffCollateralBalance.sub(premium);
    }

    // Allow repay of flash loan.
    // Just prepare for the repay of borrowedAsset. In executeOperation repaying will be occurred.
    IERC20(borrowedAsset).approve(address(LENDING_POOL), vars.flashLoanDebt);
    console.log("remainingAsset:", collateralAsset);
    console.log("remainingBalance:", vars.remainingTokens);
    console.log("remainingAssetDecimals", _getDecimals(collateralAsset));
    console.log("remainingAssetPrice", _getPrice(collateralAsset));

    // Swap remaining corrateral asset to deposit token
    if (vars.remainingTokens > 0) {
      if (collateralAsset != depositAsset){
        vars.boughtAmount = _swapExactTokensForTokens(
          collateralAsset,
          depositAsset,
          vars.remainingTokens,
          40000000000,
          useEthPath
        );
      }
      console.log("depositAsset:", depositAsset);
      //NOTE: the decimal of USDT is 6! not 18!!!
      console.log("depositAssetDecimal:", _getDecimals(depositAsset));
      console.log("depositAssetPrice:", _getPrice(depositAsset));
      console.log("boughtAmount:", vars.boughtAmount);

      // Allow this contract to deposit depositAsset
      vars.aToken = _getReserveData(depositAsset).aTokenAddress;
      IERC20(depositAsset).safeApprove(address(LENDING_POOL), 0);
      IERC20(depositAsset).safeApprove(address(LENDING_POOL), vars.boughtAmount);

      // Deposit swapped amount of USDT to LENDING_POOL.
      LENDING_POOL.deposit(depositAsset, vars.boughtAmount, user, 0);
      // Check the borrower has aToken to prove his deposit on LENDING_POOL.
      uint256 aTokenUser= IERC20(vars.aToken).balanceOf(user);
      console.log("aTokenUser:", aTokenUser);
    }
  }

  /**
   * @dev Decodes the information encoded in the flash loan params
   * @param params Additional variadic field to include extra params. Expected parameters:
   *   address collateralAsset The collateral asset to claim
   *   address borrowedAsset The asset that must be covered and will be exchanged to pay the flash loan premium
   *   address depositAsset The asset to deposit after the self-liquidation 
   *   address user The user address with a Health Factor below 1
   *   uint256 debtToCover The amount of debt to cover
   *   bool useEthPath Use WETH as connector path between the collateralAsset and borrowedAsset at Uniswap
   * @return LiquidationParams struct containing decoded params
   */
  function _decodeParams(bytes memory params) internal pure returns (LiquidationParams memory) {
    (
      address collateralAsset,
      address borrowedAsset,
      address depositAsset,
      address user,
      uint256 debtToCover,
      bool useEthPath
    ) = abi.decode(params, (address, address, address, address, uint256, bool));

    return LiquidationParams(collateralAsset, borrowedAsset, depositAsset, user, debtToCover, useEthPath);
  }

  /**
   * @dev called to trigger the Flash loan from external function in frontend Main.js
   * @param 
   *   address assets The asset to be liquidated (DAI address)
   *   uint256 amounts The amount of assets to be liquidated (DAI amount)
   *   uint256 modes Flash loan mode type, 0 means no open a debt and revert if flash loan principal is not paid. 
   *   bytes params encoded params to be structed as LiquidationParams inside executeOperation 
   */
  function requestFlashLoan(
    address[] calldata assets,
    uint256[] calldata amounts,
    uint256[] calldata modes,
    bytes calldata params
  ) external {
    // Request a Flash Loan to Lending Pool
    ILendingPool(LENDING_POOL).flashLoan(
      address(this),
      assets,
      amounts,
      modes,
      address(this),
      params,
      0
    );

    LiquidationParams memory decodedParams = _decodeParams(params);

    // aToken should be sent to the borrower address.
    // _getReserveData: get the address of aToken which is associated to the given asset. 
    address aToken = _getReserveData(decodedParams.depositAsset).aTokenAddress;
    uint256 aTokenUser = IERC20(aToken).balanceOf(decodedParams.user);
    console.log(decodedParams.user, "'s balance:", aTokenUser);
  }
}
